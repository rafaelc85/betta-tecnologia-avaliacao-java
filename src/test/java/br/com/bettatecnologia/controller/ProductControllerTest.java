package br.com.bettatecnologia.controller;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.model.ProductRelationships;
import br.com.bettatecnologia.service.ImageService;
import br.com.bettatecnologia.service.ProductRelationshipsService;
import br.com.bettatecnologia.service.ProductService;
import br.com.bettatecnologia.util.Util;

import static br.com.bettatecnologia.util.Util.asJsonString;

import java.util.ArrayList;
import java.util.List;
import org.mockito.Spy;

import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.hamcrest.Matchers; 
import org.mockito.InjectMocks; 
import org.mockito.Mock; 
import org.mockito.Mockito; 
import org.mockito.MockitoAnnotations; 
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc; 
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders; 
import org.springframework.test.web.servlet.result.MockMvcResultMatchers; 
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders; 

public class ProductControllerTest {
    
    @Mock
    ProductService service;

    @Mock
    ImageService serviceImage;

    @Mock
    ProductRelationshipsService serviceProductRelationships;

    @Mock
    MessageSource message;

    @InjectMocks
    ProductController productController;        

    @Spy
    List<Product> products = new ArrayList<>();
    
    List<Image> images;
    
    List<ProductRelationships> prList;

    private MockMvc mockMvc; 

    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();

        products = Util.getProductsList();
        images = Util.getImagesList(products); 
        prList = Util.getPrList(products,images);            
    }     

    @Test
    public void listAllProductsSuccess() throws Exception{                              
        Mockito.when(service.findAllProducts()).thenReturn(products);              
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/")) 
            .andExpect(MockMvcResultMatchers.status().isOk());                    
    }

    @Test
    public void listAllProductsWithoutProducts() throws Exception{                    
        Mockito.when(service.findAllProducts()).thenReturn(null);             
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent());                    
    }

    @Test
    public void listAllProductsIncludingRelationshipsSuccess() throws Exception{                                           
        Mockito.when(service.findAllProducts()).thenReturn(products);                                
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        Mockito.when(serviceProductRelationships.getAllProductRelationships(products,images))
                .thenReturn(prList);  

        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/ir")) 
            .andExpect(MockMvcResultMatchers.status().isOk());            
    }

    @Test
    public void listAllProductsIncludingRelationshipsWithoutRelationships() throws Exception{                                          
        Mockito.when(service.findAllProducts()).thenReturn(products);                               
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        prList = null;
        Mockito.when(serviceProductRelationships.getAllProductRelationships(products,images))
                .thenReturn(prList);                     
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/ir")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent());            
    }    

    @Test
    public void listAllProductsIncludingRelationshipsWithoutData() throws Exception{                                                           
        Mockito.when(service.findAllProducts()).thenReturn(null);              
        images = Util.getImagesList(null);                     
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        prList = null;
        Mockito.when(serviceProductRelationships.getAllProductRelationships(products,images))
                .thenReturn(prList);                     
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/ir")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent());          
    }

    @Test
    public void findAllProductsSuccess() throws Exception{                             
        Mockito.when(service.findAllProducts()).thenReturn(products);             
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/")) 
            .andExpect(MockMvcResultMatchers.jsonPath("$.[*].name", 
                    Matchers.hasItems( 
                            Matchers.endsWith("Produto1"), 
                            Matchers.endsWith("Produto2")))) 
            .andExpect(MockMvcResultMatchers.status().isOk());            
    }

    @Test
    public void getProductSuccess() throws Exception{                      
        Product product = products.get(0);            
        Mockito.when(service.findById(1L)).thenReturn(product);  
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/1")) 
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.anything("1"))) 
            .andExpect(MockMvcResultMatchers.jsonPath("$.description",Matchers.is("Descricao1"))) 
            .andExpect(MockMvcResultMatchers.jsonPath("$.name",Matchers.is("Produto1"))) 
            .andExpect(MockMvcResultMatchers.status().isOk());                     
    }

    @Test
    public void getProductWithNonExistentId() throws Exception{                               
        Mockito.when(service.findById(9L)).thenReturn(null);  
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/9")) 
            .andExpect(MockMvcResultMatchers.status().is4xxClientError());                     
    }

    @Test
    public void getProductIncludingRelationshipsSuccess() throws Exception{                               
        Product product = products.get(0);            
        Mockito.when(service.findById(1L)).thenReturn(product);                     
        Mockito.when(service.findAllProducts()).thenReturn(products);                                
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        ProductRelationships pr = Util.getPrList(products,images).get(0);
        Mockito.when(serviceProductRelationships.getSingleProductRelationships(product,products,images))
                .thenReturn(pr);  
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/ir/1")) 
            .andExpect(MockMvcResultMatchers.status().isOk()); 
    }  
    
    @Test
    public void getProductIncludingRelationshipsWithoutRelationships() throws Exception{                               
        Product product = products.get(1);            
        Mockito.when(service.findById(2L)).thenReturn(product);                   
        Mockito.when(service.findAllProducts()).thenReturn(products);                                 
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        ProductRelationships pr = null;
        Mockito.when(serviceProductRelationships.getSingleProductRelationships(product,products,images))
                .thenReturn(pr);  
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/ir/2")) 
            .andExpect(MockMvcResultMatchers.status().isOk()); 
    }  
    
    @Test
    public void getProductIncludingRelationshipsNonExistentId() throws Exception{                               
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/ir/9")) 
            .andExpect(MockMvcResultMatchers.status().is4xxClientError()); 
    }    
    
    @Test
    public void getProductSetOfChildsSuccess() throws Exception{                               
        Product product = products.get(0);            
        Mockito.when(service.findById(1L)).thenReturn(product);                   
        Mockito.when(service.findAllProducts()).thenReturn(products);                               
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        ProductRelationships pr = prList.get(0);
        Mockito.when(serviceProductRelationships.getSingleProductRelationships(product,products,images))
                .thenReturn(pr);        
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/soc/1")) 
            .andExpect(MockMvcResultMatchers.status().isOk()); 
    }  
    
    @Test
    public void getProductSetOfChildsWithoutChilds() throws Exception{                               
        Product product = products.get(1);            
        Mockito.when(service.findById(2L)).thenReturn(product);                      
        Mockito.when(service.findAllProducts()).thenReturn(products);                             
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        ProductRelationships pr = null;
        Mockito.when(serviceProductRelationships.getSingleProductRelationships(product,products,images))
                .thenReturn(pr);        
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/soc/2")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent()); 
    }  
    
    @Test
    public void getProductSetOfChildsNonExistentId() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/soc/9")) 
            .andExpect(MockMvcResultMatchers.status().is4xxClientError()); 
    }  
    
    @Test
    public void getProductSetOfImagesSuccess() throws Exception{                               
        Product product = products.get(0);            
        Mockito.when(service.findById(1L)).thenReturn(product);                   
        Mockito.when(service.findAllProducts()).thenReturn(products);                            
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        ProductRelationships pr = prList.get(0);
        Mockito.when(serviceProductRelationships.getSingleProductRelationships(product,products,images))
                .thenReturn(pr);        
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/soi/1")) 
            .andExpect(MockMvcResultMatchers.status().isOk()); 
    }  
    
    @Test
    public void getProductSetOfImagesWithoutChilds() throws Exception{                               
        Product product = products.get(1);            
        Mockito.when(service.findById(2L)).thenReturn(product);                
        Mockito.when(service.findAllProducts()).thenReturn(products);                             
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        ProductRelationships pr = null;
        Mockito.when(serviceProductRelationships.getSingleProductRelationships(product,products,images))
                .thenReturn(pr);        
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/soi/2")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent()); 
    }  
    
    @Test
    public void getProductSetOfImagesNonExistentId() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/api/product/soi/9")) 
            .andExpect(MockMvcResultMatchers.status().is4xxClientError()); 
    } 
    
    @Test
    public void deleteProductSuccess() throws Exception{
        Product product = products.get(1);            
        Mockito.when(service.findById(2L)).thenReturn(product);         
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/product/2")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent());    
    }
    
    @Test
    public void deleteProductNonExistentId() throws Exception{       
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/product/9")) 
            .andExpect(MockMvcResultMatchers.status().is4xxClientError());    
    }
    
    @Test
    public void deleteAllProductsSuccess() throws Exception{                             
        Mockito.when(service.findAllProducts()).thenReturn(products);           
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/product/")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent());    
    }
    
    @Test
    public void createProductSuccess() throws Exception{        
        Product product = products.get(0);        
        mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(product))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    }  
    
    @Test
    public void createProductDuplicatedId() throws Exception{                                     
        Mockito.when(service.findAllProducts()).thenReturn(products);           
        Product product = products.get(0);
        Mockito.when(service.findById(1L)).thenReturn(product);        
        mockMvc.perform(MockMvcRequestBuilders.post("/api/product/")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(product))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());        
    }    
    
    @Test
    public void updateProductSuccess() throws Exception{                    
        Mockito.when(service.findAllProducts()).thenReturn(products);           
        Product product = products.get(2);
        product.setName("ProdutoModificado");
        product.setDescription("DescricaoModificada");
        Mockito.when(service.findById(3L)).thenReturn(product);        
        mockMvc.perform(MockMvcRequestBuilders.put("/api/product/3")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(product))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
    
    @Test
    public void updateProductInvalidId() throws Exception{ 
        Product product = new Product();        
        mockMvc.perform(MockMvcRequestBuilders.put("/api/product/9")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(product))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError());
    }

       
}
