package br.com.bettatecnologia.controller;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.service.ImageService;
import br.com.bettatecnologia.service.ProductRelationshipsService;
import br.com.bettatecnologia.service.ProductService;
import br.com.bettatecnologia.util.Util;
import static br.com.bettatecnologia.util.Util.asJsonString;

import java.util.ArrayList;
import java.util.List;
import org.hamcrest.Matchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class ImageControllerTest {
    @Mock
    ImageService serviceImage;

    @Mock
    ProductRelationshipsService serviceProductRelationships;

    @Mock
    MessageSource message;
    
    @Mock
    ProductService serviceProduct;
    
    @InjectMocks
    ImageController imageController; 
    
    @InjectMocks
    ProductController productController;     

    @Spy
    List<Image> images = new ArrayList<>();
    
    @Spy
    List<Product> products = new ArrayList<>();
    
    private MockMvc mockMvc; 
    

    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(imageController).build(); 

        products = Util.getProductsList();    
        images = Util.getImagesList(products);
    }     

    @Test
    public void listAllImagesSuccess() throws Exception{          
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);              
        mockMvc.perform(MockMvcRequestBuilders.get("/api/image/")) 
            .andExpect(MockMvcResultMatchers.status().isOk());                    
    }
   
    @Test
    public void listAllImagesWithoutImages() throws Exception{                    
        Mockito.when(serviceImage.findAllImages()).thenReturn(null);             
        mockMvc.perform(MockMvcRequestBuilders.get("/api/image/")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent());                    
    }
    
    @Test
    public void findAllImagesSuccess() throws Exception{
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);             
        mockMvc.perform(MockMvcRequestBuilders.get("/api/image/")) 
            .andExpect(MockMvcResultMatchers.jsonPath("$.[*].type", 
                    Matchers.hasItems( 
                            Matchers.endsWith("JPG"), 
                            Matchers.endsWith("GIF")))) 
            .andExpect(MockMvcResultMatchers.status().isOk());            
    }
    
    @Test
    public void getImageSuccess() throws Exception{                               
        Mockito.when(serviceImage.findById(1L)).thenReturn(images.get(0));  
        mockMvc.perform(MockMvcRequestBuilders.get("/api/image/1")) 
            .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.anything("1"))) 
            .andExpect(MockMvcResultMatchers.jsonPath("$.type",Matchers.is("JPG"))) 
            .andExpect(MockMvcResultMatchers.jsonPath("$.product.id",Matchers.anything("1"))) 
            .andExpect(MockMvcResultMatchers.status().isOk());                     
    }  

    @Test
    public void getImageWithNonExistentId() throws Exception{                               
        Mockito.when(serviceImage.findById(9L)).thenReturn(null);  
        mockMvc.perform(MockMvcRequestBuilders.get("/api/image/9")) 
            .andExpect(MockMvcResultMatchers.status().is4xxClientError());                     
    }

    @Test
    public void deleteImageSuccess() throws Exception{          
        Mockito.when(serviceImage.findById(2L)).thenReturn(images.get(1));         
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/image/2")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent());    
    }

    @Test
    public void deleteImageNonExistentId() throws Exception{       
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/image/9")) 
            .andExpect(MockMvcResultMatchers.status().is4xxClientError());    
    }

    @Test
    public void deleteAllImagesSuccess() throws Exception{                  
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);         
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/image/")) 
            .andExpect(MockMvcResultMatchers.status().isNoContent());    
    }
    
    @Test
    public void createImageSuccess() throws Exception{    
        Image image = new Image();
        image.setProduct(products.get(0));
        image.setType("JPG");
        Mockito.when(serviceProduct.findAllProducts()).thenReturn(products);        
        mockMvc.perform(MockMvcRequestBuilders.post("/api/image/")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(image))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError());
    }  
    
    @Test
    public void createImageDuplicatedId() throws Exception{                        
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);          
        Image image = images.get(0);
        Mockito.when(serviceImage.findById(1L)).thenReturn(image);
        
        mockMvc.perform(MockMvcRequestBuilders.post("/api/image/")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(image))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError());        
    }    
    
    @Test
    public void updateImageSuccess() throws Exception{                    
        Mockito.when(serviceImage.findAllImages()).thenReturn(images);   
        
        Image image = images.get(2);
        image.setType("PSD");
        image.setProduct(products.get(1));
        Mockito.when(serviceImage.findById(3L)).thenReturn(image);
        
        mockMvc.perform(MockMvcRequestBuilders.put("/api/image/3")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(image))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
    
    @Test
    public void updateImageInvalidId() throws Exception{ 
        Image image = new Image();        
        mockMvc.perform(MockMvcRequestBuilders.put("/api/image/9")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(asJsonString(image))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError());
    }
     
}
