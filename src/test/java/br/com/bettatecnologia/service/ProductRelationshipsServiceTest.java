/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bettatecnologia.service;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.model.ProductRelationships;
import br.com.bettatecnologia.util.Util;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;


/**
 *
 * @author Rafael
 */
@RunWith(SpringRunner.class)
public class ProductRelationshipsServiceTest {   
    @InjectMocks
    private ProductRelationshipsServiceImpl prService;

    List<Product> products;
    List<Image> images;
    List<ProductRelationships> prList;   

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);        
        products = Util.getProductsList();
        images = Util.getImagesList();
        prList = Util.getPrList();
    }
    
    @Test
    public void getAllProductRelationshipsSuccess(){
        prList = prService.getAllProductRelationships(products, images);
        ProductRelationships pr = prList.get(0);
        assertThat(pr.getChilds().get(0)).isEqualTo(products.get(1)); 
        assertThat(pr.getImages().get(0)).isEqualTo(images.get(0)); 
    }   
    
    @Test
    public void getAllProductRelationshipsWithoutProducts(){
        prList = prService.getAllProductRelationships(null, images);
        assertThat(prList).isEqualTo(null); 
    }   
    
    @Test
    public void getAllProductRelationshipsWithoutImages(){
        prList = prService.getAllProductRelationships(products, null);
        assertThat(prList).isEqualTo(null); 
    }   
    
    @Test
    public void getSingleProductRelationshipsSuccess(){
        ProductRelationships pr;
        pr = prService.getSingleProductRelationships(products.get(0), products, images);
        assertThat(pr.getChilds().get(0)).isEqualTo(products.get(1)); 
        assertThat(pr.getImages().get(0)).isEqualTo(images.get(0)); 
    }   
    
    @Test
    public void getSingleProductRelationshipsWithNonExistentProduct(){
        ProductRelationships pr;
        pr = prService.getSingleProductRelationships(new Product(), products, images);
        assertThat(pr.getId()).isEqualTo(null); 
    }   
    
    @Test
    public void getSingleProductRelationshipsWithoutProduct(){
        ProductRelationships pr;
        pr = prService.getSingleProductRelationships(null, products, images);
        assertThat(pr).isEqualTo(null); 
    }   
    
    @Test
    public void getSingleProductRelationshipsWithoutProducts(){
        ProductRelationships pr;
        pr = prService.getSingleProductRelationships(products.get(0), null, images);
        assertThat(pr).isEqualTo(null); 
    }   
    
    @Test
    public void getSingleProductRelationshipsWithoutImages(){
        ProductRelationships pr;
        pr = prService.getSingleProductRelationships(products.get(0), products, null);
        assertThat(pr).isEqualTo(null); 
    }   
    
   

    
}
