/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bettatecnologia.service;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.model.ProductRelationships;
import br.com.bettatecnologia.repositories.ProductRepository;
import br.com.bettatecnologia.util.Util;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringRunner;


/**
 *
 * @author Rafael
 */
@RunWith(SpringRunner.class)
public class ProductServiceTest {   
    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;
    
    @Spy
    List<Product> products = new ArrayList<>();
    
    
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);            
        products = Util.getProductsList();  
    }
    
    @Test
    public void findById(){
        when(productService.findAllProducts()).thenReturn(products);
        Product product = products.get(0);
        when(productRepository.findOne(1L)).thenReturn(product);
        Product result = productService.findById(1L);
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getName()).isEqualTo("Produto1");     
    }
    
    @Test
    public void findByName(){
        when(productService.findAllProducts()).thenReturn(products);
        Product product = products.get(0);
        when(productRepository.findByName("Produto1")).thenReturn(product);
        Product result = productService.findByName("Produto1");
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getName()).isEqualTo("Produto1");    
    }
    
    @Test
    public void saveProduct(){
        Product product = products.get(0);
        when(productRepository.save(product)).thenReturn(product);        
        productService.saveProduct(product); 
        when(productRepository.findOne(1L)).thenReturn(product);
        Product result = productService.findById(product.getId());
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getName()).isEqualTo("Produto1");    
    }
    
    @Test
    public void updateProduct(){
        when(productService.findAllProducts()).thenReturn(products);
        Product product = products.get(0);
        product.setName("Nome-Modificado");
        productRepository.save(product);        
        productService.updateProduct(product);
        when(productRepository.findOne(1L)).thenReturn(product);
        Product result = productService.findById(1L);
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getName()).isEqualTo("Nome-Modificado");    
    }
    
    @Test
    public void deleteProductById(){
        when(productService.findAllProducts()).thenReturn(products);
        Product product = products.get(2);
        productRepository.delete(product);       
        productService.deleteProductById(product.getId());
        Product result = productService.findById(3L);
        assertThat(result).isEqualTo(null);
    }
    
    @Test
    public void deleteAllProducts(){
        when(productService.findAllProducts()).thenReturn(products);
        productRepository.deleteAll();
        productService.deleteAllProducts();
        Product result = productService.findById(3L);
        assertThat(result).isEqualTo(null);
    }
    
    @Test
    public void findAllProducts(){
        when(productService.findAllProducts()).thenReturn(products);
        when(productRepository.findAll()).thenReturn(products);
        List<Product> result = productService.findAllProducts();
        assertThat(result).isEqualTo(products);
    }
    
    @Test
    public void isProductExist(){
        when(productService.findAllProducts()).thenReturn(products);
        when(productRepository.findOne(1L)).thenReturn(products.get(0));
        Product result = productService.findById(1L);
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getName()).isEqualTo("Produto1");    
    }

    
}
