/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bettatecnologia.service;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.repositories.ImageRepository;
import br.com.bettatecnologia.util.Util;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringRunner;


/**
 *
 * @author Rafael
 */
@RunWith(SpringRunner.class)
public class ImageServiceTest {   
    @Mock
    private ImageRepository imageRepository;

    @InjectMocks
    private ImageServiceImpl imageService;

    List<Image> images;   

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        images = Util.getImagesList();
    }
    
    @Test
    public void findById(){
        when(imageService.findAllImages()).thenReturn(images);
        Image image = images.get(0);
        when(imageRepository.findOne(1L)).thenReturn(image);
        Image result = imageService.findById(1L);
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getType()).isEqualTo("JPG");     
    }
    
    @Test
    public void saveImage(){
        Image image = images.get(0);
        when(imageRepository.save(image)).thenReturn(image);        
        imageService.saveImage(image); 
        when(imageRepository.findOne(1L)).thenReturn(image);
        Image result = imageService.findById(image.getId());
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getType()).isEqualTo("JPG");    
    }
    
    @Test
    public void updateImage(){
        when(imageService.findAllImages()).thenReturn(images);
        Image image = images.get(0);
        image.setType("PSD");
        imageRepository.save(image);        
        imageService.updateImage(image);
        when(imageRepository.findOne(1L)).thenReturn(image);
        Image result = imageService.findById(1L);
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getType()).isEqualTo("PSD");    
    }
    
    @Test
    public void deleteImageById(){
        when(imageService.findAllImages()).thenReturn(images);
        Image image = images.get(2);
        imageRepository.delete(image);       
        imageService.deleteImageById(image.getId());
        Image result = imageService.findById(3L);
        assertThat(result).isEqualTo(null);
    }
    
    @Test
    public void deleteAllImages(){
        when(imageService.findAllImages()).thenReturn(images);
        imageRepository.deleteAll();
        imageService.deleteAllImages();
        Image result = imageService.findById(3L);
        assertThat(result).isEqualTo(null);
    }
    
    @Test
    public void findAllImages(){
        when(imageService.findAllImages()).thenReturn(images);
        when(imageRepository.findAll()).thenReturn(images);
        List<Image> result = imageService.findAllImages();
        assertThat(result).isEqualTo(images);
    }
    
    @Test
    public void isImageExist(){
        when(imageService.findAllImages()).thenReturn(images);
        when(imageRepository.findOne(1L)).thenReturn(images.get(0));
        Image result = imageService.findById(1L);
        assertThat(result.getId()).isEqualTo(1); 
        assertThat(result.getType()).isEqualTo("JPG");    
    }
   

    
}
