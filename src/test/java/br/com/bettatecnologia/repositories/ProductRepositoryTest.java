package br.com.bettatecnologia.repositories;

import br.com.bettatecnologia.model.Product;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import org.testng.Assert;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class ProductRepositoryTest {
    
        @Autowired
        private TestEntityManager entityManager;

        @Autowired
	private ProductRepository productRepository; 
        
        
        @Test
        public void saveProductSucess() throws Exception {           
            Product product = getSampleProduct();                       
            this.entityManager.persist(product);
            this.entityManager.flush(); 

            Product found = productRepository.findByName(product.getName());
            assertThat(product).isEqualTo(found);          
        }
        
        @Test
        public void saveProductWithInvalidProduct() throws Exception {           
            Product product = getSampleProduct();
            product.setName("");
            try{
                this.entityManager.persist(product);
            }catch (Exception e){
                assertThat(product.getId()).isEqualTo(null); 
            }                     
        }
        
        @Test
        public void updateProductSucess() throws Exception {           
            Product product = getSampleProduct();                       
            this.entityManager.persist(product);
            product.setName("Product1-Modified");
            this.entityManager.persist(product);
            
            Product found = productRepository.findByName(product.getName());
            assertThat(product).isEqualTo(found);          
        }
        
        @Test
        public void updateProductInvalidProduct() throws Exception {           
            Product product = getSampleProduct();                       
            this.entityManager.persist(product);
            product.setName("");
            this.entityManager.persist(product);
            try{
                this.entityManager.persist(product);
            }catch (Exception e){
                assertThat(product.getId()).isEqualTo(null); 
            }              
        }
  
	@Test
	public void findById(){
		Assert.assertNotNull(productRepository.findOne(1L));
		Assert.assertNull(productRepository.findOne(9L));
	}
        
        @Test
	public void findByName(){
                Product product = getSampleProduct();                       
                this.entityManager.persist(product);
		Assert.assertNotNull(productRepository.findByName("Product1"));
		Assert.assertNull(productRepository.findByName("Product99"));
	}
        
        @Test
        public void deleteProductById() throws Exception {           
            Product product = getSampleProduct();                       
            this.entityManager.persist(product);
            this.entityManager.flush(); 
            
            productRepository.delete(product.getId());
            
            Product found = productRepository.findByName(product.getName());
            Assert.assertNull(found);                    
        }
        
        @Test
        public void deleteAllProducts() throws Exception {           
            Product product = getSampleProduct();                       
            this.entityManager.persist(product);
            this.entityManager.flush(); 
            
            productRepository.deleteAll();
            Product found;
            try{
                found = productRepository.findByName(product.getName());
            }catch(Exception e){
                found = null;
            }
            Assert.assertNull(found);                    
        }

	@Test
	public void findAllProducts(){
            List<Product> products = productRepository.findAll();           
            assertThat(products.size()).isGreaterThan(1);
	}
        
        @Test
	public void isProductExists(){
            Product product = getSampleProduct();                       
            this.entityManager.persist(product);
            Assert.assertNotNull(productRepository.findOne(product.getId()));
	}               
	
	public Product getSampleProduct(){
		Product f1 = new Product();
		f1.setName("Product1");
		f1.setDescription("Description1");
		return f1;
	}

    

}
