package br.com.bettatecnologia.repositories;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.Assert;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class ImageRepositoryTest {
    
        @Autowired
        private TestEntityManager entityManager;

        @Autowired
	private ImageRepository imageRepository; 
        
        @Autowired
	private ProductRepository productRepository; 
        
        
        @Test
        public void saveImageSucess() throws Exception {           
            Image image = getSampleImage();                       
            this.entityManager.persist(image);
            this.entityManager.flush(); 

            Image found = imageRepository.findOne(image.getId());
            assertThat(image).isEqualTo(found);          
        }
        
        @Test
        public void saveImageWithInvalidImage() throws Exception {           
            Image image = getSampleImage();
            image.setProduct(null);
            try{
                this.entityManager.persist(image);
            }catch (Exception e){
                assertThat(image.getId()).isEqualTo(null); 
            }                     
        }
        
        @Test
        public void updateImageSucess() throws Exception {           
            Image image = getSampleImage();                       
            this.entityManager.persist(image);
            image.setProduct(productRepository.findOne(2L));
            this.entityManager.persist(image);
            
            Image found = imageRepository.findOne(image.getId());
            assertThat(image).isEqualTo(found);          
        }
        
        @Test
        public void updateImageInvalidImage() throws Exception {           
            Image image = getSampleImage();                       
            this.entityManager.persist(image);
            image.setProduct(null);
            this.entityManager.persist(image);
            try{
                this.entityManager.persist(image);
            }catch (Exception e){
                assertThat(image.getId()).isEqualTo(null); 
            }              
        }
  
	@Test
	public void findById(){
		Assert.assertNotNull(imageRepository.findOne(1L));
		Assert.assertNull(imageRepository.findOne(9L));
	}
        
        @Test
	public void findByType(){
                Image image = getSampleImage();                       
                this.entityManager.persist(image);
                
                List<Image> images = imageRepository.findByType("JPG");
		assertThat(images.size()).isGreaterThan(0);
	}
        
        @Test
        public void deleteImageById() throws Exception {           
            Image image = getSampleImage();                       
            this.entityManager.persist(image);
            this.entityManager.flush(); 
            
            imageRepository.delete(image.getId());
            
            Image found = imageRepository.findOne(image.getId());
            Assert.assertNull(found);                    
        }
        
        @Test
        public void deleteAllImages() throws Exception {           
            Image image = getSampleImage();                       
            this.entityManager.persist(image);
            this.entityManager.flush(); 
            
            imageRepository.deleteAll();
            Image found;
            try{
                found = imageRepository.findOne(image.getId());
            }catch(Exception e){
                found = null;
            }
            Assert.assertNull(found);                    
        }

	@Test
	public void findAllImages(){
            List<Image> images = imageRepository.findAll();           
            assertThat(images.size()).isGreaterThan(1);
	}
        
        @Test
	public void isImageExists(){
            Image image = getSampleImage();                       
            this.entityManager.persist(image);
            Assert.assertNotNull(imageRepository.findOne(image.getId()));
	}               
	
	public Image getSampleImage(){
                Product product = productRepository.findOne(1L);
            
		Image f1 = new Image();
		f1.setType("JPG");
		f1.setProduct(product);
		return f1;
	}

    

}
