INSERT INTO `product` (`id`, `name`, `description`, `parent_product_id`) VALUES
(1, 'Notebook', 'Notebook Acer', NULL),
(2, 'Mouse', 'Mouse Microsoft', 1),
(3, 'Computador Desktop', 'Computador Core 2 Duo', NULL),
(4, 'Caixa de Som', 'Caixa de Som sem fio', 3),
(5, 'Fone de Ouvido', 'Fone sem fio', 1);

INSERT INTO `image` (`id`, `type`, `product_id`) VALUES
(1, 'JPG', 1),
(2, 'GIF', 1),
(3, 'PNG', 1),
(4, 'JPG', 2),
(5, 'PSD', 3);
