'use strict';

angular.module('crudApp').controller('HomeController',
    ['HomeService', '$scope',  function( HomeService, $scope) {

        var self = this;
        self.product = {};
        self.products=[];
        self.image = {};
        self.images=[];

        self.searchFor = searchFor;
        self.productClear = productClear;
        self.submitProduct = submitProduct;
        self.getAllProducts = getAllProducts;
        self.getProduct = getProduct;
        self.createProduct = createProduct;
        self.updateProduct = updateProduct;
        self.removeProduct = removeProduct;
        self.editProduct = editProduct;
        self.submitImage = submitImage;
        self.getAllImages = getAllImages;
        self.createImage = createImage;
        self.updateImage = updateImage;
        self.removeImage = removeImage;
        self.editImage = editImage;
        self.reset = reset;

        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;
        self.action = '';
        self.showInfo = false;
        self.showChilds = false;
        self.showImages = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;       

        function submitProduct() {
            console.log('Submitting');
            if (self.product.id === undefined || self.product.id === null) {
                console.log('Saving New Product', self.product);
                createProduct(self.product);
            } else {
                updateProduct(self.product, self.product.id);
                console.log('Product updated with id ', self.product.id);
            }
        }
        
        function submitImage() {
            console.log('Submitting');
            if (self.image.id === undefined || self.image.id === null) {
                console.log('Saving New Image', self.image);
                createImage(self.image);
            } else {
                updateImage(self.image, self.image.id);
                console.log('Product updated with id ', self.image.id);
            }
        }
        
        function productClear(){  
            console.log('Clear Product'); 
                        
            self.showInfo = false;
            self.showChilds = false;
            self.showImages = false;
        }
        
        function searchFor(action) {
            console.log('Search For');
            getProduct(self.product.id);            
            switch (action) {
                case 'c':       
                    self.showInfo = true;
                    self.showChilds = false;
                    self.showImages = false;
                    break;
                case 'd':
                    self.showInfo = true;
                    self.showChilds = true;
                    self.showImages = true;
                    break;
                case 'e':
                    self.showInfo = false;
                    self.showChilds = true;
                    self.showImages = false;
                    break;
                case 'f':
                    self.showInfo = false;
                    self.showChilds = false;
                    self.showImages = true;
                    break;
            }
        }
        
        function getProduct(id) {
            HomeService.getProduct(id).then(
                function (product) {
                    self.product = product;
                },
                function (errResponse) {
                    console.error('Error while getting Product ' + id + ', Error :' + errResponse.data.errorMessage);
                    self.product.name = null;
                    self.product.description = null;
                    self.product.parent = null;
                    self.product={};
                }
            );
        }
        
        function createProduct(product) {
            console.log('About to create Product'); 
            
            HomeService.createProduct(product)
                .then(
                    function (response) {
                        console.log('Product created successfully');
                        self.successMessage = 'Product created successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.product={};
                        $scope.formProduto.$setPristine();
                    },
                    function (errResponse) {
                        console.error('Error while creating Product');
                        self.errorMessage = 'Error while creating Product: ' + errResponse.data.errorMessage;                        
                        self.successMessage='';
                    }
                );
        }
        
        function createImage(image) {
            console.log('About to create Image');             
            HomeService.createImage(image)
                .then(
                    function (response) {
                        console.log('Image created successfully');
                        self.successMessage = 'Image created successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.image={};
                        $scope.formImagem.$setPristine();
                    },
                    function (errResponse) {
                        console.error('Error while creating Image');
                        self.errorMessage = 'Error while creating Image: ' + errResponse.data.errorMessage;                        
                        self.successMessage='';
                    }
                );
        }


        function updateProduct(product, id){
            console.log('About to update product');
            HomeService.updateProduct(product, id)
                .then(
                    function (response){
                        console.log('Product updated successfully');
                        self.successMessage='Product updated successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.product={};
                        $scope.formProduto.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while updating Product');
                        self.errorMessage='Error while updating Product '+ errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }
        
        function updateImage(image, id){
            console.log('About to update Image');
            HomeService.updateImage(image, id)
                .then(
                    function (response){
                        console.log('Image updated successfully');
                        self.successMessage='Image updated successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.image={};
                        $scope.formImagem.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while updating Product');
                        self.errorMessage='Error while updating Product '+ errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }


        function removeProduct(id){
            console.log('About to remove Product with id '+id);
            HomeService.removeProduct(id)
                .then(
                    function(){
                        console.log('Product '+id + ' removed successfully');
                        self.successMessage='Product removed successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.product={};
                        $scope.formImagem.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while removing Product '+id +', Error :'+ errResponse.data.errorMessage);
                        self.errorMessage='Error while removing Product '+id +', Error :'+ errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }
        
        function removeImage(id){
            console.log('About to remove Image with id '+id);
            HomeService.removeImage(id)
                .then(
                    function(){
                        console.log('Image '+id + ' removed successfully');
                        self.successMessage='Image '+id + ' removed successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.image={};
                        $scope.formImagem.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while removing Image '+id +', Error :'+ errResponse.data.errorMessage);
                        self.errorMessage='Error while removing Image '+id +', Error :'+ errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }

        function getAllProducts(){
            return HomeService.getAllProducts();
        }
        
        function getAllImages(){
            return HomeService.getAllImages();
        }        

        function editProduct(id) {
            self.successMessage='';
            self.errorMessage='';
            HomeService.getProduct(id).then(
                function (product) {
                    self.product = product;
                },
                function (errResponse) {
                    console.error('Error while editing Product ' + id + ', Error :' + errResponse.data.errorMessage);
                    self.errorMessage='Error while editing Product '+id +', Error :'+ errResponse.data.errorMessage;
                    self.successMessage='';
                }
            );
        }
        
        function editImage(id) {
            self.successMessage='';
            self.errorMessage='';
            HomeService.getImage(id).then(
                function (image) {
                    self.image = image;
                },
                function (errResponse) {
                    console.error('Error while editing Image ' + id + ', Error :' + errResponse.data.errorMessage);
                    self.errorMessage='Error while editing Image '+id +', Error :'+ errResponse.data.errorMessage;
                    self.successMessage='';
                }
            );
        }
        
        function reset(){
            self.successMessage='';
            self.errorMessage='';
            self.product={};
            self.image={};
            $scope.formProduto.$setPristine(); 
            $scope.formImagem.$setPristine(); 
        }
    }


    ]);