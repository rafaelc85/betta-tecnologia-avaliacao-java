'use strict';

angular.module('crudApp').factory('HomeService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
                loadAllProducts: loadAllProducts,                
                getAllProducts: getAllProducts,                
                getProduct: getProduct,
                createProduct: createProduct,
                updateProduct: updateProduct,
                removeProduct: removeProduct,
                loadAllImages: loadAllImages,
                getAllImages: getAllImages,
                getImage: getImage,
                createImage: createImage,
                updateImage: updateImage,
                removeImage: removeImage
            };

            return factory;

            function loadAllProducts() {
                console.log('Fetching all Products');
                var deferred = $q.defer();
                $http.get(urls.PRODUCT_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all Products');
                            $localStorage.products = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading Products');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function loadAllImages() {
                console.log('Fetching all Images');
                var deferred = $q.defer();
                $http.get(urls.IMAGE_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all Images');
                            $localStorage.images = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading Images');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function getAllProducts(){
                return $localStorage.products;
            }
            
            function getAllImages(){
                return $localStorage.images;
            }            

            function getProduct(id) {
                console.log('Fetching Product with id :'+id);
                var deferred = $q.defer();
                $http.get(urls.PRODUCT_SERVICE_API + id)
                    .then(
                        function (response) {
                            console.log('Fetched successfully Product with id :'+id);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while loading Product with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function getImage(id) {
                console.log('Fetching Image with id :'+id);
                var deferred = $q.defer();
                $http.get(urls.IMAGE_SERVICE_API + id)
                    .then(
                        function (response) {
                            console.log('Fetched successfully Image with id :'+id);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while loading Image with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function createProduct(product) {
                console.log('Creating Product');
                var deferred = $q.defer();  
                
                $http.post(urls.PRODUCT_SERVICE_API, product)
                    .then(
                        function (response) {
                            loadAllProducts();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                           console.error('Error while creating Product : '+errResponse.data.errorMessage);
                           deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function createImage(image) {
                console.log('Creating Image');
                var deferred = $q.defer();  
                
                $http.post(urls.IMAGE_SERVICE_API, image)
                    .then(
                        function (response) {
                            loadAllImages();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                           console.error('Error while creating Image : '+errResponse.data.errorMessage);
                           deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function updateProduct(product, id) {
                console.log('Updating Product with id '+id);
                var deferred = $q.defer();
                $http.put(urls.PRODUCT_SERVICE_API + id, product)
                    .then(
                        function (response) {
                            loadAllProducts();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while updating Product with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function updateImage(image, id) {
                console.log('Updating Image with id '+id);
                var deferred = $q.defer();
                $http.put(urls.IMAGE_SERVICE_API + id, image)
                    .then(
                        function (response) {
                            loadAllImages();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while updating Image with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            function removeProduct(id) {
                console.log('Removing Product with id '+id);
                var deferred = $q.defer();
                $http.delete(urls.PRODUCT_SERVICE_API + id)
                    .then(
                        function (response) {
                            loadAllProducts();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while removing Product with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function removeImage(id) {
                console.log('Removing Image with id '+id);
                var deferred = $q.defer();
                $http.delete(urls.IMAGE_SERVICE_API + id)
                    .then(
                        function (response) {
                            loadAllImages();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while removing Image with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

        }
    ]);