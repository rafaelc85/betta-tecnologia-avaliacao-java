var app = angular.module('crudApp',['ui.router','ngStorage']);

app.constant('urls', {
    BASE: 'http://localhost:8080/BettaTecnologiaAvaliacaoJavaApp',
    PRODUCT_SERVICE_API : 'http://localhost:8080/BettaTecnologiaAvaliacaoJavaApp/api/product/',
    IMAGE_SERVICE_API : 'http://localhost:8080/BettaTecnologiaAvaliacaoJavaApp/api/image/'
});

app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', 
            {
                url: '/',
                views: {
                    '': {
                        templateUrl: 'partials/list',
                        controller:'HomeController',
                        controllerAs:'ctrl',
                        resolve: {
                            products: function ($q, HomeService) {
                                console.log('Load all products');
                                var deferred = $q.defer();
                                HomeService.loadAllProducts().then(deferred.resolve, deferred.resolve);                                
                                return deferred.promise;
                            },
                            images: function ($q, HomeService) {
                                console.log('Load all images');
                                var deferred = $q.defer();                                
                                HomeService.loadAllImages().then(deferred.resolve, deferred.resolve);
                                return deferred.promise;
                            }
                        }
                    }                    
                }
            })        
            ;
        $urlRouterProvider.otherwise('/');

    }]);

