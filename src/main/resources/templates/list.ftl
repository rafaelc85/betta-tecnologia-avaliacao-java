<div class="generic-container">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Product Add or Update</span></div>
            <div class="panel-body">
	        <div class="formcontainer">
	            <div class="alert alert-success" role="alert" ng-if="ctrl.successMessage">{{ctrl.successMessage}}</div>
	            <div class="alert alert-danger" role="alert" ng-if="ctrl.errorMessage">{{ctrl.errorMessage}}</div>
	            <form ng-submit="ctrl.submitProduct()" name="formProduto" class="form-horizontal">
	                <input type="hidden" ng-model="ctrl.product.id" />
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="pname">Name</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.product.name" id="pname" class="username form-control input-sm" placeholder="Enter Product Name" required ng-minlength="3"/>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="pdescription">Description</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.product.description" id="pdescription" class="username form-control input-sm" placeholder="Enter Product Description" required ng-minlength="3"/>
	                        </div>
	                    </div>
	                </div>
	
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="parent">Parent Product</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.product.parent.id" id="pparent" class="form-control input-sm" placeholder="Enter Parent Product Parent ID" ng-pattern="ctrl.onlyNumbers"/>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-actions floatRight">
	                        <input type="submit"  value="{{!ctrl.product.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="formProduto.$invalid || formProduto.$pristine">
	                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="formProduto.$pristine">Reset Form</button>
	                    </div>
	                </div>
	            </form>
    	    </div>
        </div>
        <div class="panel-heading"><span class="lead">Image Add or Update</span></div>
            <div class="panel-body">
	        <div class="formcontainer">	    
	            <form ng-submit="ctrl.submitImage()" name="formImagem" class="form-horizontal">
	                <input type="hidden" ng-model="ctrl.image.id" />
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="pname">Type</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.image.type" id="ptype" class="username form-control input-sm" placeholder="Enter Image Type" required ng-minlength="3"/>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="pdescription">Product ID</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.image.product.id" id="pproductId" class="username form-control input-sm" placeholder="Enter Product ID" required ng-pattern="ctrl.onlyNumbers"/>
	                        </div>
	                    </div>
	                </div>

	                <div class="row">
	                    <div class="form-actions floatRight">
	                        <input type="submit"  value="{{!ctrl.image.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="formImagem.$invalid || formImagem.$pristine">
	                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="formImagem.$pristine">Reset Form</button>
	                    </div>
	                </div>
	            </form>
    	    </div>
        </div>		
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Item 2 (a) - Products excluding relationships</span></div>
		<div class="panel-body">
			<div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>NAME</th>
                                    <th>DESCRIPTION</th>
                                    <th>PARENT_PRODUCT</th>
                                    <th width="100"></th>
                                    <th width="100"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="u in ctrl.getAllProducts()">
                                    <td>{{u.id}}</td>
                                    <td>{{u.name}}</td>
                                    <td>{{u.description}}</td>
                                    <td>{{u.parent.name}}</td>
                                    <td><button type="button" ng-click="ctrl.editProduct(u.id)" class="btn btn-success custom-width">Edit</button></td>
                                    <td><button type="button" ng-click="ctrl.removeProduct(u.id)" class="btn btn-danger custom-width">Remove</button></td>
                                </tr>
                                </tbody>
                            </table>	
			</div>
		</div>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Item 2 (b) - Products including relationships</span></div>
		<div class="panel-body">
			<div class="table-responsive">
                            <table class="table table-hover">
                                <tbody>
                                <tr ng-repeat="u in ctrl.getAllProducts()">
                                    <td><b>ID:</b> {{u.id}}</td>
                                    <td><b>NAME:</b> {{u.name}}</td>
                                    <td><b>DESCRIPTION:</b> {{u.description}}</td>
                                    <td><b>PARENT:</b> {{u.parent.name}}</td>
                                    <td><b>CHILDS:</b></td>
                                    <td ng-repeat="p in ctrl.getAllProducts()" ng-if="p.parent.id == u.id">
                                        {{p.name}}; 
                                    </td>
                                    <td><b>IMAGES:</b></td> 
                                    
                                    <td ng-repeat="i in ctrl.getAllImages()" ng-if="i.product.id == u.id">
                                        {{i.id}}; 
                                    </td>   
                                    
                                   
                                </tr>
                                </tbody>
                            </table>	
			</div>
		</div>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Images</span></div>
		<div class="panel-body">
			<div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>TYPE</th>
                                    <th>PRODUCT</th>
                                    <th width="100"></th>
                                    <th width="100"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="u in ctrl.getAllImages()">
                                    <td>{{u.id}}</td>
                                    <td>{{u.type}}</td>
                                    <td>{{u.product.name}}</td>
                                    <td><button type="button" ng-click="ctrl.editImage(u.id)" class="btn btn-success custom-width">Edit</button></td>
                                    <td><button type="button" ng-click="ctrl.removeImage(u.id)" class="btn btn-danger custom-width">Remove</button></td>
                                </tr>
                                </tbody>
                            </table>	
			</div>
		</div>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">Item 2 (c)(d)(e)(f) - Search Information by Product Identity</span></div>
		<div class="panel-body">
                    <form name="formSearch" class="form-horizontal">
	                <div class="row">
	                    <div class="form-group col-md-12">
	                        <label class="col-md-2 control-lable" for="pname">Product ID</label>
	                        <div class="col-md-7">
	                            <input type="text" ng-model="ctrl.product.id" id="pname" class="username form-control input-sm" placeholder="Enter Product ID" required ng-pattern="ctrl.onlyNumbers" ng-keyup="ctrl.productClear()"/>
	                        </div>
	                    </div>
	                </div>

	                <center>
                        <div class="row">
	                    <div class="form-actions">	                        
	                        <button type="button" ng-click="ctrl.searchFor('c')" class="btn btn-primary btn-sm" ng-disabled="formSearch.$pristine">(c) Search Product Excluding Relationships</button>                                
                                <button type="button" ng-click="ctrl.searchFor('d')" class="btn btn-primary btn-sm" ng-disabled="formSearch.$pristine">(d) Search Product Including Relationships</button>                                
                                <button type="button" ng-click="ctrl.searchFor('e')" class="btn btn-primary btn-sm" ng-disabled="formSearch.$pristine">(e) Get Set of childs</button>                                
                                <button type="button" ng-click="ctrl.searchFor('f')" class="btn btn-primary btn-sm" ng-disabled="formSearch.$pristine">(f) Get Set of images</button>                                
                            </div>
	                </div>
                        </center>
	            </form>

                   <div>
                        <div><b>ID: </b>{{ctrl.product.id}}</div>
                        <div ng-show="ctrl.showInfo"><b>Name: </b>{{ctrl.product.name}}</div>
                        <div ng-show="ctrl.showInfo"><b>Description: </b>{{ctrl.product.description}}</div>
                        <div ng-show="ctrl.showInfo"><b>Parent: </b>{{ctrl.product.parent.name}}</div>
                        <div ng-show="ctrl.showChilds"><b>Childs: </b>
                            <div ng-repeat="p in ctrl.getAllProducts()" ng-if="p.parent.id == ctrl.product.id">
                                {{p.name}}; 
                            </div>
                        </div>
                        <div ng-show="ctrl.showImages"><b>Images: </b>
                            <div ng-repeat="i in ctrl.getAllImages()" ng-if="i.product.id == ctrl.product.id">
                                {{i.id}}; 
                            </div>
                        </div>
                   </div>
		</div>
    </div>
</div>