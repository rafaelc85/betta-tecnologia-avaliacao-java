package br.com.bettatecnologia.controller;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import br.com.bettatecnologia.service.ImageService;
import br.com.bettatecnologia.service.ProductService;
import br.com.bettatecnologia.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class ImageController {

	public static final Logger logger = LoggerFactory.getLogger(ImageController.class);

	@Autowired
	ImageService imageService; 
        
        @Autowired
	ProductService productService; 

	// -------------------Retrieve All Images---------------------------------------------

	@RequestMapping(value = "/image/", method = RequestMethod.GET)
	public ResponseEntity<List<Image>> listAllImages() {
		List<Image> images = imageService.findAllImages();
		if (images == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Image>>(images, HttpStatus.OK);
	}

	// -------------------Retrieve Single Image------------------------------------------

	@RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getImage(@PathVariable("id") long id) {
		logger.info("Fetching Image with id {}", id);
		Image image = imageService.findById(id);
		if (image == null) {
			logger.error("Image with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Image with id " + id 
					+ " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Image>(image, HttpStatus.OK);
	}

	// -------------------Create a Image-------------------------------------------

	@RequestMapping(value = "/image/", method = RequestMethod.POST)
	public ResponseEntity<?> createImage(@RequestBody Image image, UriComponentsBuilder ucBuilder) {
            
		logger.info("Creating Image : {}", image);                 
                
                Product product = productService.findById(image.getProduct().getId());                
                if (product == null) {
			logger.error("Product with id {} not found.", image.getProduct().getId());
			return new ResponseEntity(new CustomErrorType("Product with id " + 
                                image.getProduct().getId() + " not found"), HttpStatus.NOT_FOUND);
		}                
              
		imageService.saveImage(image);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/image/{id}").buildAndExpand(image.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ------------------- Update a Image ------------------------------------------------

	@RequestMapping(value = "/image/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateImage(@PathVariable("id") long id, @RequestBody Image image) {
		logger.info("Updating Image with id {}", id);

		Image currenImage = imageService.findById(id);

		if (currenImage == null) {
			logger.error("Unable to update. Image with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to upate. Image with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		currenImage.setType(image.getType());
		currenImage.setProduct(image.getProduct());

		imageService.updateImage(currenImage);
		return new ResponseEntity<Image>(currenImage, HttpStatus.OK);
	}

	// ------------------- Delete a Image-----------------------------------------

	@RequestMapping(value = "/image/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteImage(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Image with id {}", id);

		Image image = imageService.findById(id);
		if (image == null) {
			logger.error("Unable to delete. Image with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Image with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		imageService.deleteImageById(id);
		return new ResponseEntity<Image>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Delete All Images-----------------------------

	@RequestMapping(value = "/image/", method = RequestMethod.DELETE)
	public ResponseEntity<Image> deleteAllImages() {
		logger.info("Deleting All Images");

		imageService.deleteAllImages();
		return new ResponseEntity<Image>(HttpStatus.NO_CONTENT);
	}

}