package br.com.bettatecnologia.controller;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.model.ProductRelationships;
import br.com.bettatecnologia.service.ImageService;
import br.com.bettatecnologia.service.ProductRelationshipsService;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import br.com.bettatecnologia.service.ProductService;
import br.com.bettatecnologia.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class ProductController {

	public static final Logger logger = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	ProductService productService; 
        
        @Autowired
	ImageService imageService; 
        
        @Autowired
	ProductRelationshipsService productRelationshipsService;

	// -------------------Retrieve All Products Excluding Relationships-----------------------

	@RequestMapping(value = "/product/", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> listAllProducts() {
		List<Product> products = productService.findAllProducts();
		if (products == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
	}
        
        // -------------Retrieve All Products Including Relationships-----------------------------

	@RequestMapping(value = "/product/ir", method = RequestMethod.GET)
	public ResponseEntity<List<ProductRelationships>> listAllProductsIncludingRelationships() {
		List<Product> products = productService.findAllProducts();
                if (products == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
                List<Image> images = imageService.findAllImages();
                List<ProductRelationships> prList;
                prList = productRelationshipsService.getAllProductRelationships(products,images);
		if (prList == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<ProductRelationships>>(prList, HttpStatus.OK);
	}
      
        // -------Retrieve Single Product excluding relationships-----------------------

	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getProduct(@PathVariable("id") long id) {
		logger.info("Fetching Product with id {}", id);
		Product product = productService.findById(id);
		if (product == null) {
			logger.error("Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Product with id " + id 
					+ " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Product>(product, HttpStatus.OK);
	}
        
        // -------Retrieve Single Product including relationships-----------------------

	@RequestMapping(value = "/product/ir/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getProductIncludingRelationships(@PathVariable("id") long id) {
		logger.info("Fetching Product with id {}", id);
		Product product = productService.findById(id);
		if (product == null) {
			logger.error("Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Product with id " + id 
					+ " not found"), HttpStatus.NOT_FOUND);
		}
                List<Product> products = productService.findAllProducts();
                List<Image> images = imageService.findAllImages();
                ProductRelationships pr;
                pr = productRelationshipsService.getSingleProductRelationships(product,products,images);                
		
                return new ResponseEntity<ProductRelationships>(pr, HttpStatus.OK);
	}
        
        // --------------------Retrieve Single Product set of childs-----------------------

	@RequestMapping(value = "/product/soc/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Product>> getProductSetOfChilds(@PathVariable("id") long id) {
		logger.info("Fetching Product with id {}", id);
		Product product = productService.findById(id);
		if (product == null) {
                    logger.error("Product with id {} not found.", id);
                    return new ResponseEntity(new CustomErrorType("Product with id " + id 
                                    + " not found"), HttpStatus.NOT_FOUND);
		}
                List<Product> products = productService.findAllProducts();
                List<Image> images = imageService.findAllImages();
                ProductRelationships pr;
                pr = productRelationshipsService.getSingleProductRelationships(product,products,images);
		
                if (pr == null) {
                    logger.error("Product with id {} doesnt have childs.", id);
                    return new ResponseEntity(new CustomErrorType("Product with id " + id 
                                    + " doesnt have childs"), HttpStatus.NO_CONTENT);
		}
                
                return new ResponseEntity<List<Product>>(pr.getChilds(), HttpStatus.OK);
	}
        
        // --------------------Retrieve Single Product set of images-----------------------

	@RequestMapping(value = "/product/soi/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Image>> getProductSetOfImages(@PathVariable("id") long id) {
		logger.info("Fetching Product with id {}", id);
		Product product = productService.findById(id);
		if (product == null) {
			logger.error("Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Product with id " + id 
					+ " not found"), HttpStatus.NOT_FOUND);
		}
                List<Product> products = productService.findAllProducts();
                List<Image> images = imageService.findAllImages();
                ProductRelationships pr;
                pr = productRelationshipsService.getSingleProductRelationships(product,products,images);
                
                if (pr == null) {
                    logger.error("Product with id {} doesnt have child images.", id);
                    return new ResponseEntity(new CustomErrorType("Product with id " + id 
                                    + " doesnt have child images"), HttpStatus.NO_CONTENT);
		}
		
                return new ResponseEntity<List<Image>>(pr.getImages(), HttpStatus.OK);
	}

	// -------------------Create a Product-------------------------------------------

	@RequestMapping(value = "/product/", method = RequestMethod.POST)
	public ResponseEntity<?> createProduct(@RequestBody Product product, UriComponentsBuilder ucBuilder) {
            
		logger.info("Creating Product : {}", product); 
                
                if(product.getParent() != null)                
                    product.setParent(productService.findById(product.getParent().getId()));

		if (productService.isProductExist(product)) {
			logger.error("Unable to create. A Product with name {} already exist", product.getName());
			return new ResponseEntity(new CustomErrorType("Unable to create. A Product with name " + 
			product.getName() + " already exist."),HttpStatus.CONFLICT);
		}
		productService.saveProduct(product);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/product/{id}").buildAndExpand(product.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ------------------- Update a Product ------------------------------------------------

	@RequestMapping(value = "/product/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateProduct(@PathVariable("id") long id, @RequestBody Product product) {
		logger.info("Updating Product with id {}", id);

		Product currenProduct = productService.findById(id);

		if (currenProduct == null) {
			logger.error("Unable to update. Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to upate. Product with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		currenProduct.setName(product.getName());
		currenProduct.setDescription(product.getDescription());
		
                if(product.getParent().getId() != null)                
                    currenProduct.setParent(productService.findById(product.getParent().getId()));
                else
                    currenProduct.setParent(null);
                
                if(productService.isChild(product, currenProduct)){
                    logger.error("Unable to update. Unexpected Error");
			return new ResponseEntity(new CustomErrorType("Unable to update. Unexpected Error"),
					HttpStatus.NOT_FOUND);
                }

                productService.updateProduct(currenProduct);
                return new ResponseEntity<Product>(currenProduct, HttpStatus.OK);		
	}

	// ------------------- Delete a Product-----------------------------------------

	@RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteProduct(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Product with id {}", id);

		Product product = productService.findById(id);
		if (product == null) {
			logger.error("Unable to delete. Product with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Product with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		try{
                    productService.deleteProductById(id);
                }catch (Exception e) {
                    logger.error("Unable to delete. Product with id " + id + " has dependencies and cant be removed.");
			return new ResponseEntity(new CustomErrorType("Unable to delete. Product with id " + id + " has dependencies and cant be removed."),
					HttpStatus.NOT_FOUND);
                }
		return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Delete All Products-----------------------------

	@RequestMapping(value = "/product/", method = RequestMethod.DELETE)
	public ResponseEntity<Product> deleteAllProducts() {
		logger.info("Deleting All Products");

		productService.deleteAllProducts();
		return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
	}

}