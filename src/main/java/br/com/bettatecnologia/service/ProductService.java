package br.com.bettatecnologia.service;

import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.repositories.ProductRepository;

import java.util.List;

public interface ProductService {
	
	Product findById(Long id);

	Product findByName(String name);

	void saveProduct(Product product);

	void updateProduct(Product product);

	void deleteProductById(Long id);

	void deleteAllProducts();

	List<Product> findAllProducts();

	boolean isProductExist(Product product);
        
        boolean isChild(Product product1, Product product2); 

}