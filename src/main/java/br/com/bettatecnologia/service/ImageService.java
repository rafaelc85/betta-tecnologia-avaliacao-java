package br.com.bettatecnologia.service;

import br.com.bettatecnologia.model.Image;

import java.util.List;

public interface ImageService {
	
	Image findById(Long id);

	List<Image> findByType(String type);

	void saveImage(Image image);

	void updateImage(Image image);

	void deleteImageById(Long id);

	void deleteAllImages();

	List<Image> findAllImages();

	boolean isImageExist(Image image);
}