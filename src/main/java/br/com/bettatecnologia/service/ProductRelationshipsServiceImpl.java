package br.com.bettatecnologia.service;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.model.ProductRelationships;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service("productRelationshipsService")
public class ProductRelationshipsServiceImpl implements ProductRelationshipsService{

    @Override
    public List<ProductRelationships> getAllProductRelationships(List<Product> products,
            List<Image> images) {
        
        if(products == null || products.isEmpty() || images == null) return null;
        
        List<ProductRelationships> prList = new ArrayList<>();
        for(int i=0;i<products.size();i++){
            //add product information
            prList.add(new ProductRelationships(products.get(i)));
            
            //add child Images 
            for(int j=0;j<images.size();j++){
                if(products.get(i).getId().equals(images.get(j).getProduct().getId())){
                    prList.get(i).getImages().add(images.get(j));
                }
            }
            
            //add child Products
            for(int j=0;j<products.size();j++){
                if(products.get(j).getParent() != null){
                    if(products.get(i).getId().equals(products.get(j).getParent().getId())){
                        prList.get(i).getChilds().add(products.get(j));
                    }
                }
            }            
        }        
        return prList;
    }

    @Override
    public ProductRelationships getSingleProductRelationships(Product product, List<Product> products,
            List<Image> images) {
        
        if(product == null || products == null || products.isEmpty() || images == null) return null;
        
        //add product information
        ProductRelationships pr = new ProductRelationships(product);
        
        //add child images
        for(int j=0;j<images.size();j++){
            if(images.get(j).getProduct().getId().equals(product.getId())){
                pr.getImages().add(images.get(j));
            }
        }    
        
        //add child products
        for(int j=0;j<products.size();j++){
            if(products.get(j).getParent() != null){
                if(products.get(j).getParent().getId().equals(product.getId())){
                    pr.getChilds().add(products.get(j));
                }
            }
        }      
        
        return pr;
    }


}




