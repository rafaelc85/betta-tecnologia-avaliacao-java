package br.com.bettatecnologia.service;

import br.com.bettatecnologia.model.Image;
import java.util.List;

import br.com.bettatecnologia.repositories.ImageRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("imageService")
@Transactional
public class ImageServiceImpl implements ImageService{

	@Autowired
	private ImageRepository imageRepository;

	public Image findById(Long id) {
		return imageRepository.findOne(id);
	}

	public List<Image> findByType(String type) {
            List<Image> images = imageRepository.findAll();
            List<Image> found = new ArrayList<>();
            for(int i=0; i<images.size(); i++){
                if(images.get(i).getType().equals(type)){ 
                    found.add(images.get(i));
                }
            }            
            return found;
	}

	public void saveImage(Image image) {
		imageRepository.save(image);
	}

	public void updateImage(Image image){
		saveImage(image);
	}

	public void deleteImageById(Long id){
		imageRepository.delete(id);
	}

	public void deleteAllImages(){
		imageRepository.deleteAll();
	}

	public List<Image> findAllImages(){
		return imageRepository.findAll();
	}

	public boolean isImageExist(Image image) {
		return findById(image.getId()) != null;
	}

}
