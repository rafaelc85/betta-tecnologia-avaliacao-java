package br.com.bettatecnologia.service;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.model.ProductRelationships;

import java.util.List;

public interface ProductRelationshipsService {

	List<ProductRelationships> getAllProductRelationships(List<Product> products,
            List<Image> images);
        ProductRelationships getSingleProductRelationships(Product product, List<Product> products,
            List<Image> images);
               

}