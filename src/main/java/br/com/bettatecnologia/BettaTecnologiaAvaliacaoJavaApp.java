package br.com.bettatecnologia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import br.com.bettatecnologia.configuration.JpaConfiguration;


@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"br.com.bettatecnologia"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
public class BettaTecnologiaAvaliacaoJavaApp {

	public static void main(String[] args) {
		SpringApplication.run(BettaTecnologiaAvaliacaoJavaApp.class, args);
	}
}

