package br.com.bettatecnologia.model;

import java.util.ArrayList;
import java.util.List;

public class ProductRelationships extends Product{
    private List<Image> images;
    private List<Product> childs;
    
    public ProductRelationships(Product product){
        super.setId(product.getId());
        super.setName(product.getName());
        super.setDescription(product.getDescription());
        super.setParent(product.getParent());
        images = new ArrayList<>();
        childs = new ArrayList<>();
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<Product> getChilds() {
        return childs;
    }

    public void setChilds(List<Product> childs) {
        this.childs = childs;
    }    
       

}
