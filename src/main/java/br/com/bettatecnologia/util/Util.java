/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.bettatecnologia.util;

import br.com.bettatecnologia.model.Image;
import br.com.bettatecnologia.model.Product;
import br.com.bettatecnologia.model.ProductRelationships;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rafael
 */
public class Util {
    
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            System.out.println(jsonContent);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public static List<Product> getProductsList() {
        List<Product> products = new ArrayList<>();
        Product p1,p2,p3;
        
        p1 = new Product();
        p1.setId(1L);
        p1.setDescription("Descricao1");
        p1.setName("Produto1"); 
        
        p2 = new Product();
        p2.setId(2L);
        p2.setDescription("Descricao2");
        p2.setName("Produto2");
        p2.setParent(p1); 
        
        p3 = new Product();
        p3.setId(3L);
        p3.setDescription("Descricao3");
        p3.setName("Produto3");
        p3.setParent(p1);
        
        products.add(p1);
        products.add(p2);
        products.add(p3);
        return products;
    }
    
    public static List<Image> getImagesList(){
        return getImagesList(getProductsList());
    }

    public static List<Image> getImagesList(List<Product> products) {
        if (products == null || products.isEmpty()) return null;
        
        List<Image> images = new ArrayList<>();        
        Image i1,i2,i3;
        
        i1 = new Image();
        i1.setId(1L);
        i1.setProduct(products.get(0));
        i1.setType("JPG");
        
        i2 = new Image();
        i2.setId(2L);
        i2.setProduct(products.get(0));
        i2.setType("GIF");
        
        i3 = new Image();
        i3.setId(3L);
        i3.setProduct(products.get(0));
        i3.setType("PNG");
        
        images.add(i1);
        images.add(i2);
        images.add(i3);
        
        return images;
    }
    
    public static List<ProductRelationships> getPrList(){
        return getPrList(getProductsList(), getImagesList(getProductsList()));
    }

    public static List<ProductRelationships> getPrList(List<Product> products, List<Image> images) {
        List<ProductRelationships> prList = new ArrayList<>();
        List<Product> childProducts = new ArrayList<>();
        List<Image> childImages = new ArrayList<>();
                
        prList.add(new ProductRelationships(products.get(0)));
        childProducts.add(products.get(1));
        childProducts.add(products.get(2));
        childImages.add(images.get(0));
        childImages.add(images.get(1));
        childImages.add(images.get(2));
        prList.get(0).setChilds(childProducts);
        prList.get(0).setImages(childImages);        
        
        return prList;
    }

}  
    

